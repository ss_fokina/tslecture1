import { IUserCustom } from "./interfaces";
import { IUser } from "./interfaces";
import {usersInfoArray} from "./userInfo";
import {usersArray} from "./users";


function getUsersJobPositions(usersArray: IUser[]): IUserCustom[]{
    const newUserArray: IUserCustom[] = [];

    if (Array.isArray(usersArray)) {
        usersArray.forEach((elUser) => {
            const userInfo = usersInfoArray.find(el => el.userid === elUser.userid);
            if (userInfo) {
                const newUserObject: IUserCustom = {
                    name: userInfo.name,
                    position: userInfo.organization.position,
                    age: userInfo.age,
                    gender: elUser.gender
                }
                newUserArray.push(newUserObject);
            }
        })
    }

    return newUserArray;
}
const usersPositions = getUsersJobPositions(usersArray);
console.log('userPositions', usersPositions)