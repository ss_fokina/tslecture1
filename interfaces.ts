
export interface IUserCustom {
    name: string,
    position: string,
    age: number,
    gender: string
}
export interface IUser {
    userid: string,
    name: string,
    gender: string
}
interface IOrganization {
    name: string,
    position: string
}
export interface IUsersInfo {
    userid: string,
    name: string,
    birthdate: string,
    age: number,
    organization: IOrganization
}